<?php

namespace Drupal\Tests\setka_editor\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\setka_editor\Controller\SetkaEditorApiController;
use Drupal\setka_editor\SetkaEditorHelper;

/**
 * Tests Setka Editor module with Style Manager interaction functions.
 *
 * @group setka_editor
 */
class SetkaEditorApiTest extends KernelTestBase {
  private $testToken = 'TestSetkaEditorToken';

  private $testConfig = [
    'setka_editor_version' => 'TestVersion',
    'setka_editor_public_token' => 'TestPublicToken',
    'setka_company_meta_data' => 'TestCompanyJson',
    'setka_editor_js_cdn' => 'TestEditorJs',
    'setka_editor_css_cdn' => 'TestEditorCss',
    'setka_company_css_cdn' => 'TestCompanyCss',
    'setka_company_json_cdn' => 'TestCompanyJson',
    'setka_public_js_cdn' => 'TestPublicJs',
  ];

  private $testStyleManagerResponse;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'file',
    'setka_editor',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->testStyleManagerResponse = [
      'content_editor_version' => $this->testConfig['setka_editor_version'],
      'public_token' => $this->testConfig['setka_editor_public_token'],
      'content_editor_files' => [
        [
          'filetype' => 'js',
          'url' => $this->testConfig['setka_editor_js_cdn'],
        ],
        [
          'filetype' => 'css',
          'url' => $this->testConfig['setka_editor_css_cdn'],
        ],
      ],
      'theme_files' => [
        [
          'filetype' => 'json',
          'url' => $this->testConfig['setka_company_json_cdn'],
        ],
        [
          'filetype' => 'css',
          'url' => $this->testConfig['setka_company_css_cdn'],
        ],
      ],
      'plugins' => [
        [
          'filetype' => 'js',
          'url' => $this->testConfig['setka_public_js_cdn'],
        ],
      ],
    ];
  }

  /**
   * Tests push API callback function.
   */
  public function testPushEditorConfigCdn() {
    $testConfig = $this->testConfig;

    \Drupal::configFactory()->getEditable('setka_editor.settings')
      ->set('setka_license_key', $this->testToken)
      ->set('setka_use_cdn', TRUE)
      ->save();

    $data['content_editor_version'] = $testConfig['setka_editor_version'];
    $data['public_token'] = $testConfig['setka_editor_public_token'];
    $data['content_editor_files'][0]['filetype'] = 'js';
    $data['content_editor_files'][0]['url'] = $testConfig['setka_editor_js_cdn'];
    $data['content_editor_files'][1]['filetype'] = 'css';
    $data['content_editor_files'][1]['url'] = $testConfig['setka_editor_css_cdn'];
    $data['theme_files'][0]['filetype'] = 'json';
    $data['theme_files'][0]['url'] = $testConfig['setka_company_json_cdn'];
    $data['theme_files'][1]['filetype'] = 'css';
    $data['theme_files'][1]['url'] = $testConfig['setka_company_css_cdn'];
    $data['plugins'][0]['filetype'] = 'js';
    $data['plugins'][0]['url'] = $testConfig['setka_public_js_cdn'];

    $setkaEditorApiController = new SetkaEditorApiController(
      $this->container->get('setka_editor.api'),
      $this->container->get('config.factory'),
      $this->container->get('tempstore.private'),
      $this->container->get('file.usage'),
      $this->container->get('database'),
      $this->container->get('cache.discovery'),
      $this->container->get('asset.css.collection_optimizer'),
      $this->container->get('asset.js.collection_optimizer'),
      $this->container->get('file_system'),
      $this->container->get('queue'),
      $this->container->get('library.discovery'),
      $this->container->get('lock')
    );

    $setkaEditorApiController->processEditorConfig($this->testToken, $data);
    $setkaEditorSettings = \Drupal::config('setka_editor.settings');

    foreach ($this->testConfig as $configName => $expectedValue) {
      $configValue = $setkaEditorSettings->get($configName);
      $this->assertEquals($expectedValue, $configValue);
    }
  }

  /**
   * Tests updateSetkaEditorConfig SetkaEditorHelper method.
   */
  public function testUpdateEditorConfigMethod() {
    $testConfig = $this->testConfig;
    $testConfig['setka_license_key'] = $this->testToken;
    $testConfig['setka_use_cdn'] = TRUE;

    $editableConfig = \Drupal::configFactory()->getEditable('setka_editor.settings');
    SetkaEditorHelper::updateSetkaEditorConfig($editableConfig, $testConfig, \Drupal::state());

    $setkaEditorSettings = \Drupal::config('setka_editor.settings');
    foreach ($this->testConfig as $configName => $expectedValue) {
      $configValue = $setkaEditorSettings->get($configName);
      $this->assertEquals($expectedValue, $configValue);
    }
  }

  /**
   * Tests parseStyleManagerData SetkaEditorHelper method.
   */
  public function testParseStyleManagerDataMethod() {
    $parsedValues = SetkaEditorHelper::parseStyleManagerData($this->testStyleManagerResponse);
    foreach ($this->testConfig as $configName => $expectedValue) {
      $configValue = !empty($parsedValues[$configName]) ? $parsedValues[$configName] : NULL;
      $this->assertEquals($expectedValue, $configValue);
    }
  }

}
