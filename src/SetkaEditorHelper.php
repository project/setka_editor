<?php

namespace Drupal\setka_editor;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Setka Editor helper service.
 */
class SetkaEditorHelper {

  use StringTranslationTrait;

  const SETKA_EDITOR_TOOLBAR_OFFSET = 80;
  const SETKA_EDITOR_DOMAIN = 'https://editor.setka.io';
  const SETKA_EDITOR_SUPPORT = self::SETKA_EDITOR_DOMAIN . '/support';
  const SETKA_EDITOR_JSON_CONFIG = 'setka_company_json';
  const SETKA_EDITOR_CDN_ASSETS_PATTERN = '/https:\/\/ceditor\.setka\.io\/clients\/([^\/]*)\/css\/assets/';
  const SETKA_EDITOR_CDN_CSS_PATTERN = '/https:\/\/ceditor\.setka\.io\/clients\/(.*?)\/css\//';

  /**
   * Drupal messenger interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger, ConfigFactory $configFactory) {
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
  }

  /**
   * Returns current SetkaEditor API base URL.
   *
   * @return string
   *   Current SetkaEditor API base URL.
   */
  public function getSetkaEditorApiDomain() {

    if (empty($setkaEditorApiDomain = Settings::get('setka_api_domain'))) {
      $setkaEditorApiDomain = self::SETKA_EDITOR_DOMAIN;
    }

    return $setkaEditorApiDomain;
  }

  /**
   * Checks if setka editor meta data contains.
   *
   * @param array|null $metaData
   *   Setka editor layouts and themes data.
   * @param string $postTheme
   *   Post theme id.
   * @param string $postGrid
   *   Post layout id.
   *
   * @return array
   *   Check results array.
   */
  public function checkPostMeta($metaData, $postTheme, $postGrid) {
    if (!isset($postTheme) && !isset($postGrid)) {
      return ['postTheme' => TRUE, 'postGrid' => TRUE];
    }
    $result = ['postTheme' => FALSE, 'postGrid' => FALSE];
    if ($metaData) {
      if (!is_array($metaData)) {
        $metaData = $this->parseSetkaEditorMeta($metaData);
      }
      if (!empty($metaData['layouts'])) {
        $result['postGrid'] = in_array($postGrid, $metaData['layouts']);
      }
      if (!empty($metaData['themes'])) {
        $result['postTheme'] = in_array($postTheme, $metaData['themes']);
      }
    }

    if (!$result['postTheme'] || !$result['postGrid']) {
      $setkaSupportLink = Link::fromTextAndUrl(
        $this->t('Setka Editor team'),
        Url::fromUri(self::SETKA_EDITOR_SUPPORT, ['attributes' => ['target' => '_blank']])
      )->toString();
      $this->messenger->addError(
        $this->t("Setka Editor can't be launched because Style or Grid System were removed from the Style Manager or you've changed your license key. Please contact @link.", ['@link' => $setkaSupportLink])
      );
    }

    return $result;
  }

  /**
   * Parses Style Manager data to array.
   *
   * @param array|null $currentBuild
   *   Current build data.
   *
   * @return array
   *   Parsed data array.
   */
  public static function parseStyleManagerData($currentBuild) {
    $values = [];
    if (!empty($currentBuild['content_editor_version'])) {
      $values['setka_editor_version'] = $currentBuild['content_editor_version'];
      $values['setka_editor_public_token'] = $currentBuild['public_token'];
      foreach ($currentBuild['content_editor_files'] as $fileData) {
        switch ($fileData['filetype']) {
          case 'js':
            $values['setka_editor_js_cdn'] = $fileData['url'];
            break;

          case 'css':
            $values['setka_editor_css_cdn'] = $fileData['url'];
            break;
        }
      }
      foreach ($currentBuild['theme_files'] as $fileData) {
        switch ($fileData['filetype']) {
          case 'css':
            $values['setka_company_css_cdn'] = $fileData['url'];
            break;

          case 'json':
            $values['setka_company_json_cdn'] = $fileData['url'];
            $values['setka_company_meta_data'] = $fileData['url'];
            break;
        }
      }
      foreach ($currentBuild['plugins'] as $fileData) {
        switch ($fileData['filetype']) {
          case 'js':
            $values['setka_public_js_cdn'] = $fileData['url'];
            break;
        }
      }

      $values['setka_editor_standalone_styles'] = $currentBuild['standalone_styles'];
    }
    return $values;
  }

  /**
   * Downloads and returns internal file URL.
   *
   * @param string $fileUrl
   *   File CDN URL.
   * @param bool $changeCdnUrls
   *   If cdn urls should be replaced with local.
   * @param string $filename
   *   File name.
   *
   * @return string
   *   Internal file URL.
   */
  public static function downloadSetkaEditorFile($fileUrl, $changeCdnUrls = FALSE, $filename = NULL) {
    $fileData = file_get_contents($fileUrl);
    if ($fileData) {
      if ($changeCdnUrls) {
        $setkaFilesUrl = \Drupal::service('stream_wrapper_manager')->getViaUri('public://setka/assets')->getExternalUrl();
        $fileData = preg_replace(self::SETKA_EDITOR_CDN_ASSETS_PATTERN, $setkaFilesUrl, $fileData);
      }
      if (empty($filename)) {
        preg_match('/.*?\/([^\/]+)$/', $fileUrl, $matches);
        $filename = $matches[1];
      }
      if ($filename) {
        $file = file_save_data($fileData, 'public://setka/' . $filename, FileSystemInterface::EXISTS_REPLACE);
        return $file ? file_create_url($file->getFileUri()) : FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Downloads and returns internal file URL.
   *
   * @param string $fileUrl
   *   File CDN URL.
   *
   * @return string
   *   Internal file URL.
   */
  public static function downloadIconFontFile($fileUrl) {
    $fileData = file_get_contents($fileUrl);
    if ($fileData) {
      preg_match('/.*?\/([^\/]+)$/', $fileUrl, $matches);
      $filename = $matches[1];
      $directoryPath = preg_replace(self::SETKA_EDITOR_CDN_CSS_PATTERN, '', $fileUrl);
      $directoryPath = str_replace('/' . $filename, '', $directoryPath);
      $directoryPath = "public://setka/{$directoryPath}";
      \Drupal::service('file_system')->prepareDirectory($directoryPath, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $file = file_save_data($fileData, "{$directoryPath}/{$filename}", FileSystemInterface::EXISTS_REPLACE);

      return $file ? file_create_url($file->getFileUri()) : FALSE;
    }

    return FALSE;
  }

  /**
   * Checks if public://setka is writable directory.
   *
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   Drupal file system service.
   *
   * @return bool
   *   Directory is writable - TRUE, else - FALSE
   */
  public static function checkSetkaFolderPermissions(FileSystem $fileSystem) {
    $directory = $fileSystem->realpath('public://setka');
    $is_writable = is_writable($directory);
    $is_directory = is_dir($directory);
    return ($is_writable && $is_directory);
  }

  /**
   * Sets task of URLs to download.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|\Drupal\Core\Config\Config $config
   *   Drupal config object.
   * @param \Drupal\Core\State\State $drupalState
   *   Drupal state service.
   * @param array $newSettings
   *   New Style Manager settings.
   */
  public static function buildSetkaFilesUpdateTask($config, State $drupalState, array $newSettings) {
    $updateTask = $drupalState->get('setka_update_task');
    if (empty($updateTask)) {
      $updateTask = [];
    }

    $styleManagerSettingsRequired = [
      'setka_company_css',
      'setka_company_json',
    ];
    foreach ($styleManagerSettingsRequired as $settingName) {
      $newValue = $newSettings[$settingName . '_cdn'];
      $updateTask[$settingName] = $newValue;
    }
    $styleManagerSettings = [
      'setka_editor_js',
      'setka_editor_css',
      'setka_public_js',
    ];
    foreach ($styleManagerSettings as $settingName) {
      $currentValue = $config->get($settingName . '_cdn');
      $newValue = $newSettings[$settingName . '_cdn'];
      if (!$drupalState->get($settingName) || $currentValue != $newValue) {
        $updateTask[$settingName] = $newValue;
      }
    }
    $updateTask['setka_editor_standalone_styles'] = $newSettings['setka_editor_standalone_styles'];
    $updateTask['setka_icons_and_fonts'] = $newSettings['icons_and_fonts'];
    $drupalState->set('setka_update_task', $updateTask);
  }

  /**
   * This method updates Style Editor files on server storage.
   *
   * @param \Drupal\Core\State\State $drupalState
   *   Drupal state service.
   */
  public static function runSetkaFilesUpdateTask(State $drupalState) {
    $updateTask = $drupalState->get('setka_update_task');
    if (empty($updateTask)) {
      return [];
    }

    $downloadFile = function ($fileUrl, $configName, $updateTask, &$result, $filename = NULL) {
      if ($configName) {
        $changeCdnUrls = $configName === self::SETKA_EDITOR_JSON_CONFIG;
        if ($localFileUrl = self::downloadSetkaEditorFile($fileUrl, $changeCdnUrls, $filename)) {
          $result[$configName] = $localFileUrl;
          unset($updateTask[$configName]);
        }
        else {
          $result[$configName] = FALSE;
          \Drupal::logger('setka_editor')->error('Unable to download file: @url', ['@url' => $fileUrl]);
        }
      }
    };

    foreach ($updateTask['setka_icons_and_fonts'] as $item) {
      if (in_array($item['filetype'], ['font', 'image'])) {
        self::downloadIconFontFile($item['url']);
      }
    }

    $result = [];
    foreach ($updateTask as $configName => $fileUrl) {
      if (!in_array($configName, ['setka_editor_standalone_styles', 'setka_icons_and_fonts'])) {
        $downloadFile($fileUrl, $configName, $updateTask, $result);
      }
    }

    foreach ($updateTask['setka_editor_standalone_styles'] as $type => $styles) {
      if (in_array($type, ['common', 'themes', 'layouts'])) {
        foreach ($styles as $style) {
          $configName = 'setka_editor_' . $type . '_' . $style['id'];
          $filename = $type . '_' . $style['id'] . '.' . $style['filetype'];
          $downloadFile($style['url'], $configName, $updateTask, $result, $filename);
        }
      }
    }

    if (!empty($updateTask)) {
      $drupalState->set('setka_update_task', $updateTask);
    }
    else {
      $drupalState->delete('setka_update_task');
    }

    $drupalState->setMultiple($result);
  }

  /**
   * Returns max upload file size in bytes or 0 if unlimited.
   *
   * @return int
   *   Max upload file size in bytes.
   */
  public static function getUploadMaxSize() {
    $uploadMaxSize = self::getInBytes(ini_get('post_max_size'));
    $uploadMaxFileSize = self::getInBytes(ini_get('upload_max_filesize'));
    if (!$uploadMaxFileSize || ($uploadMaxSize && $uploadMaxFileSize > $uploadMaxSize)) {
      $uploadMaxFileSize = $uploadMaxSize;
    }
    return (int) $uploadMaxFileSize;
  }

  /**
   * Returns param value in bytes.
   *
   * @param string $val
   *   Php.ini param.
   *
   * @return bool|int
   *   Param value in bytes or FALSE.
   */
  public static function getInBytes($val) {
    $val = trim($val);
    if (empty($val)) {
      return FALSE;
    }
    $last = strtolower($val[strlen($val) - 1]);
    switch ($last) {
      case 'g':
        $val = (int) $val * 1024;
      case 'm':
        $val = (int) $val * 1024;
      case 'k':
        $val = (int) $val * 1024;
    }
    return $val;
  }

  /**
   * Download, parse and save to config Setka Editor meta data.
   *
   * @param string $url
   *   Meta data file URL.
   *
   * @return array
   *   Meta data array.
   */
  public function parseSetkaEditorMeta($url) {
    $setkaCompanyMetaData = ['layouts' => [], 'themes' => []];
    if ($metaDataJson = file_get_contents($url)) {
      $metaData = Json::decode($metaDataJson);
      if (!empty($metaData['assets']['layouts'])) {
        foreach ($metaData['assets']['layouts'] as $layout) {
          $setkaCompanyMetaData['layouts'][] = $layout['id'];
        }
      }
      if (!empty($metaData['assets']['themes'])) {
        foreach ($metaData['assets']['themes'] as $theme) {
          $setkaCompanyMetaData['themes'][] = $theme['id'];
        }
      }
      $this->configFactory->getEditable('setka_editor.settings')
        ->set('setka_company_meta_data', $setkaCompanyMetaData)
        ->save();
    }
    return $setkaCompanyMetaData;
  }

  /**
   * Checks if Setka Editor version less then 2.0.
   *
   * @return bool
   *   Version check result.
   */
  public function isSetkaEditorVersionLess2() {
    $setkaEditorVersion = $this->configFactory->get('setka_editor.settings')->get('setka_editor_version');
    return !empty($setkaEditorVersion) && version_compare('2.0', $setkaEditorVersion, '>');
  }

  /**
   * Updates all Setka Editor settings in DB.
   *
   * @param \Drupal\Core\Config\Config $setkaEditorConfig
   *   Editable Setka Editor config.
   * @param array $newSettings
   *   New settings values.
   * @param \Drupal\Core\State\State $state
   *   Drupal state service.
   */
  public static function updateSetkaEditorConfig(Config $setkaEditorConfig, array $newSettings, State $state) {
    if (isset($newSettings['setka_license_key'])) {
      $setkaEditorConfig->set('setka_license_key', $newSettings['setka_license_key']);
    }
    if (isset($newSettings['setka_use_cdn'])) {
      $setkaEditorConfig->set('setka_use_cdn', $newSettings['setka_use_cdn']);
    }

    $setkaEditorConfig
      ->set('setka_editor_version', $newSettings['setka_editor_version'])
      ->set('setka_editor_type_of_integration', $newSettings['setka_editor_type_of_integration'])
      ->set('setka_editor_public_token', $newSettings['setka_editor_public_token'])
      ->set('setka_company_meta_data', $newSettings['setka_company_meta_data'])
      ->set('setka_editor_js_cdn', $newSettings['setka_editor_js_cdn'])
      ->set('setka_editor_css_cdn', $newSettings['setka_editor_css_cdn'])
      ->set('setka_company_css_cdn', $newSettings['setka_company_css_cdn'])
      ->set('setka_company_json_cdn', $newSettings['setka_company_json_cdn'])
      ->set('setka_public_js_cdn', $newSettings['setka_public_js_cdn'])
      ->save();

    $standaloneStyles = [];
    foreach ($newSettings['setka_editor_standalone_styles'] as $type => $styles) {
      if (in_array($type, ['common', 'themes', 'layouts'])) {
        foreach ($styles as $style) {
          $localConfigName = 'setka_editor_' . $type . '_' . $style['id'];
          $configName = $localConfigName . '_cdn';
          $standaloneStyles[$type][] = [
            'local' => $localConfigName,
            'cdn' => $configName,
            'cdnUrl' => $style['url'],
            'id' => $style['id'],
          ];

          $state->set($configName, $style['url']);
        }
      }
    }

    $state->set('setka_editor_standalone_styles', $standaloneStyles);

  }

}
