<?php

namespace Drupal\setka_editor\Form;

use Drupal\Core\Asset\CssCollectionOptimizer;
use Drupal\Core\Asset\JsCollectionOptimizer;
use Drupal\Core\Asset\LibraryDiscovery;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfirmFormInterface;
use Drupal\Core\Link;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\setka_editor\SetkaEditorApi;
use Drupal\setka_editor\SetkaEditorHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Setka Editor settings.
 */
class SettingsForm extends ConfigFormBase  implements ConfirmFormInterface {

  /**
   * Setka Editor api service.
   *
   * @var \Drupal\setka_editor\SetkaEditorApi
   */
  protected $editorApi;

  /**
   * Cache Discovery bin backend.
   *
   * @var \Drupal\Core\Cache\DatabaseBackend
   */
  protected $cacheDiscovery;

  /**
   * Drupal CSS optimizer service.
   *
   * @var \Drupal\Core\Asset\CssCollectionOptimizer
   */
  protected $cssOptimizer;

  /**
   * Drupal JS optimizer service.
   *
   * @var \Drupal\Core\Asset\JsCollectionOptimizer
   */
  protected $jsOptimizer;

  /**
   * Drupal file_system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Drupal queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryDiscovery;

  /**
   * Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  protected $step = 1;
  protected $values = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory,
                              SetkaEditorApi $editorApi,
                              CacheBackendInterface $cacheDiscovery,
                              CssCollectionOptimizer $cssOptimizer,
                              JsCollectionOptimizer $jsOptimizer,
                              FileSystem $fileSystem,
                              State $state,
                              QueueFactory $queueFactory,
                              LibraryDiscovery $libraryDiscovery,
                              LockBackendInterface $lock,
                              ModuleHandler $moduleHandler) {
    parent::__construct($configFactory);
    $this->editorApi = $editorApi;
    $this->cacheDiscovery = $cacheDiscovery;
    $this->cssOptimizer = $cssOptimizer;
    $this->jsOptimizer = $jsOptimizer;
    $this->fileSystem = $fileSystem;
    $this->state = $state;
    $this->queueFactory = $queueFactory;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->lock = $lock;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('setka_editor.api'),
      $container->get('cache.discovery'),
      $container->get('asset.css.collection_optimizer'),
      $container->get('asset.js.collection_optimizer'),
      $container->get('file_system'),
      $container->get('state'),
      $container->get('queue'),
      $container->get('library.discovery'),
      $container->get('lock'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'setka_editor_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'setka_editor.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->step === 2) {
      $form['#title'] = $this->getQuestion();

      $form['#attributes']['class'][] = 'confirmation';
      $form['description'] = ['#markup' => $this->getDescription()];
      $form[$this->getFormName()] = ['#type' => 'hidden', '#value' => 1];

      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->getConfirmText(),
        '#button_type' => 'primary',
      ];

      $form['actions']['cancel'] = [
        '#type' => 'link',
        '#title' => $this->getCancelText(),
        '#attributes' => ['class' => ['button']],
        '#url' => $this->getCancelUrl(),
        '#cache' => [
          'contexts' => [
            'url.query_args:destination',
          ],
        ],
      ];;

      // By default, render the form using theme_confirm_form().
      if (!isset($form['#theme'])) {
        $form['#theme'] = 'confirm_form';
      }
      return $form;
    }

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('setka_editor.settings');
    $setkaEditorVersion = $config->get('setka_editor_version');

    if ($this->moduleHandler->moduleExists('amp') && !$this->moduleHandler->moduleExists('setka_editor_amp')) {
      $hash = $this->moduleHandler->moduleExists('module_filter') ? 'other' : 'edit-modules-other';
      $form['missed_setka_editor_amp'] = [
        '#markup' => '<div class="messages messages--warning">' . $this->t('If you have the <a href="https://www.drupal.org/project/amp">AMP</a> module enabled, enable the <a href="/admin/modules#@hash">Setka Editor AMP</a> submodule.', ['@hash' => $hash]) . '</div>',
      ];
    }

    if ($setkaEditorVersion) {
      $form['setka_license_description'] = [
        '#markup' => '<div class="messages messages--status">' . $this->t('Current Setka Editor version: @version', ['@version' => $setkaEditorVersion]) . '</div>',
      ];
    }
    else {
      $link = Link::fromTextAndUrl(SetkaEditorHelper::SETKA_EDITOR_DOMAIN, Url::fromUri(SetkaEditorHelper::SETKA_EDITOR_DOMAIN))->toString();
      $licenseDescription = $this->t('To activate Setka Editor you need to register at @link. After registration, you will receive a unique license key, which must be inserted in the box below. If you already have a license key, use it to activate it.',
        ['@link' => $link]
      );
      $form['setka_license_description'] = [
        '#markup' => '<p>' . $licenseDescription . '</p>',
      ];
    }

    $form['setka_license_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('License key'),
      '#default_value' => $config->get('setka_license_key'),
      '#empty_value' => '',
      '#maxlength' => 255,
      '#required' => FALSE,
      '#description' => $this->t('You may find the license key in your personal account.'),
    ];

    $setkaUseCdn = $config->get('setka_use_cdn');
    $setkaUseCdnDisabled = !SetkaEditorHelper::checkSetkaFolderPermissions($this->fileSystem);
    $form['setka_use_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use files from Setka CDN'),
      '#default_value' => $setkaUseCdn,
      '#disabled' => $setkaUseCdnDisabled,
      '#description' => $this->t('If option is checked module will use css/js files from setka.io cdn, otherwise files will be loaded on site.'),
    ];

    $description = [
      t('<strong>Default.</strong> Only load styles needed for a specific Setka Editor post.'),
      '<br>',
      t('All Setka Editor styles will be loaded on the page as separate files.'),
      t('This provides a balance between user experience and speed.'),
      '<br><br>',
      t('<strong>Legacy.</strong> Load a combined CSS file with all your Setka Editor styles.'),
      '<br>',
      t('This will increase the assets\' size and might affect load speeds.'),
      t('Use only if you plan on displaying Setka Editor posts in different styles (configured on editor.setka.io) on a single page.')
    ];

    $setkaIntegrationType = $config->get('setka_editor_type_of_integration');
    $form['setka_editor_type_of_integration'] = [
      '#type' => 'radios',
      '#options' => [
        'default' => $this->t('Default'),
        'legacy' => $this->t('Legacy'),
      ],
      '#title' => $this->t('Type of integration'),
      '#default_value' => $setkaIntegrationType,
      '#description' => implode(' ', $description),
    ];

    $imageStyles = ImageStyle::loadMultiple();
    $imageStylesOptions = [];
    $setkaImageStyles = $config->get('setka_editor_image_styles');
    foreach ($imageStyles as $imageStyle) {
      $imageStylesOptions[$imageStyle->id()] = $imageStyle->label();
    }
    $imageStyleDescription = [
      t('When you delete the image style settings used in the content created in Setka Editor in @link, all the images that have been generated for this style will be permanently deleted.', [
        '@link' => Link::createFromRoute(Url::fromRoute('entity.image_style.collection')->toString(), 'entity.image_style.collection')->toString(),
      ]),
      t('If no replacement style is selected, the dependent configurations might need manual reconfiguration.'),
    ];
    $form['setka_editor_image_styles'] = [
      '#type' => 'checkboxes',
      '#description' => implode(' ', $imageStyleDescription),
      '#options' => $imageStylesOptions,
      '#title' => $this->t('Enabled image styles'),
      '#default_value' => $setkaImageStyles ? array_intersect($setkaImageStyles, array_keys($imageStylesOptions)) : [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->step === 2) {
      return;
    }
    parent::validateForm($form, $form_state);
    $setka_license_key = $form_state->getValue('setka_license_key');
    if (!empty($setka_license_key)) {
      if (!$this->editorApi->getCurrentBuild($setka_license_key)) {
        $link = $this->getLinkGenerator()->generate(SetkaEditorHelper::SETKA_EDITOR_SUPPORT, Url::fromUri(SetkaEditorHelper::SETKA_EDITOR_SUPPORT));
        $licenseText = $this->t('It seems that something went wrong. Contact customer support: @link.',
          ['@link' => $link]
        );
        $form_state->setErrorByName('setka_license_key', $licenseText);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->step === 1) {
      $form_state->setRebuild();
      $this->step = 2;
      $this->values = $form_state->getValues();
      unset($this->values['submit'], $this->values['op']);
      return;
    }
    $values = $this->values;
    $originalLicenseKey = $this->config('setka_editor.settings')->get('setka_license_key');

    $downloadFiles = (!$values['setka_use_cdn'] && SetkaEditorHelper::checkSetkaFolderPermissions($this->fileSystem));
    if (!empty($values['setka_license_key'])) {
      if ($currentBuild = $this->editorApi->getCurrentBuild($values['setka_license_key'])) {
        $parsedValues = SetkaEditorHelper::parseStyleManagerData($currentBuild);
        $values = array_merge($values, $parsedValues);
      }
    }
    if (!empty($values['setka_editor_js_cdn']) && !empty($values['setka_editor_css_cdn']) &&
      !empty($values['setka_company_css_cdn']) && !empty($values['setka_company_json_cdn'] &&
        !empty($values['setka_public_js_cdn']))) {
      if (empty($originalLicenseKey) || ($originalLicenseKey != $values['setka_license_key'])) {
        $this->editorApi->pushSystemInfo($values['setka_license_key']);
      }
      if ($downloadFiles) {
        $iconsAndFonts = $this->editorApi->getIconsFonts($values['setka_license_key']);
        if (!empty($iconsAndFonts)) {
          $values['icons_and_fonts'] = $iconsAndFonts;
        }
        $queue = $this->queueFactory->get('update_setka_editor');
        if (!$queue->numberOfItems()) {
          $queue->createQueue();
        }
        $queue->createItem(['newSettings' => $values]);

        if ($this->lock->acquire('setka_editor_files_update')) {

          while ($newSettingsItem = $queue->claimItem()) {
            $newSettingsData = $newSettingsItem->data['newSettings'];
            SetkaEditorHelper::buildSetkaFilesUpdateTask($this->config('setka_editor.settings'), $this->state, $newSettingsData);

            $editableConfig = $this->configFactory->getEditable('setka_editor.settings');
            SetkaEditorHelper::updateSetkaEditorConfig($editableConfig, $newSettingsData, $this->state);

            $this->libraryDiscovery->clearCachedDefinitions();
            $this->configFactory->reset('setka_editor.settings');
            SetkaEditorHelper::runSetkaFilesUpdateTask($this->state);
            $queue->deleteItem($newSettingsItem);
          }

          parent::submitForm($form, $form_state);

          foreach (Cache::getBins() as $cache_backend) {
            $cache_backend->deleteAll();
          }
          $this->libraryDiscovery->clearCachedDefinitions();
          $this->configFactory->reset('setka_editor.settings');
          $this->cacheDiscovery->deleteAll();
          $this->cssOptimizer->deleteAll();
          $this->jsOptimizer->deleteAll();
          _drupal_flush_css_js();

          $this->lock->release('setka_editor_files_update');
        }
        else {
          $editableConfig = $this->configFactory->getEditable('setka_editor.settings');

          if (isset($values['setka_license_key'])) {
            $editableConfig->set('setka_license_key', $values['setka_license_key']);
          }
          if (isset($values['setka_use_cdn'])) {
            $editableConfig->set('setka_use_cdn', $values['setka_use_cdn']);
          }

          $editableConfig->set('setka_editor_type_of_integration', $values['setka_editor_type_of_integration']);

          $editableConfig->save();
        }
      }
      else {
        $editableConfig = $this->configFactory->getEditable('setka_editor.settings');
        SetkaEditorHelper::updateSetkaEditorConfig($editableConfig, $values, $this->state);

        $this->state->setMultiple(
          [
            'setka_editor_js' => FALSE,
            'setka_editor_css' => FALSE,
            'setka_company_css' => FALSE,
            'setka_company_json' => FALSE,
            'setka_public_js' => FALSE,
          ]
        );
        parent::submitForm($form, $form_state);
        foreach (Cache::getBins() as $cache_backend) {
          $cache_backend
            ->deleteAll();
        }
        $this->libraryDiscovery->clearCachedDefinitions();
        $this->configFactory->reset('setka_editor.settings');
        $this->cacheDiscovery->deleteAll();
        $this->cssOptimizer->deleteAll();
        $this->jsOptimizer->deleteAll();
        _drupal_flush_css_js();
      }

      if (empty($originalLicenseKey)) {
        $this->messenger()->addMessage($this->t('Setka Editor license key activated successfully!'));
      }
      elseif ($originalLicenseKey != $values['setka_license_key']) {
        $this->messenger()->addMessage($this->t('Setka Editor license key updated successfully!'));
      }
      else {
        $this->messenger()->addMessage($this->t('Setka Editor configuration updated successfully!'));
      }
    }
    else {
      $editableConfig = $this->config('setka_editor.settings');

      $editableConfig->delete()->save();

      $editableConfig
        ->set('setka_license_key', $values['setka_license_key'])
        ->set('setka_use_cdn', $values['setka_use_cdn'])
        ->set('setka_editor_type_of_integration', $values['setka_editor_type_of_integration'])
        ->set('setka_editor_image_styles', $values['setka_editor_image_styles'])
        ->save();
    }

    $editableConfig = $this->config('setka_editor.settings');
    $editableConfig
      ->set('setka_editor_image_styles', $values['setka_editor_image_styles'])
      ->save();
  }

  /**
   * Returns CDN Setka Editor config value if local is empty.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Immutable config.
   * @param \Drupal\Core\State\State $state
   *   Drupal state service.
   * @param string $confName
   *   Config value name.
   * @param bool $libraryFormat
   *   TRUE - returns library formatted array, FALSE - returns URL string.
   * @param bool $loadAsync
   *   Load library asynchronously or not.
   *
   * @return string|null
   *   Config value.
   */
  public static function getConfigValue(ImmutableConfig $config, State $state, $confName, $libraryFormat = FALSE, $loadAsync = FALSE) {
    $attributes = [];
    if ($loadAsync) {
      $attributes = ['async' => TRUE];
    }

    $setkaUseCdn = TRUE;
    if (\Drupal::lock()->lockMayBeAvailable('setka_editor_files_update')) {
      $setkaUseCdn = $config->get('setka_use_cdn');
    }

    if (!$setkaUseCdn) {
      $confValue = $state->get($confName);
      if (empty($confValue)) {
        $confValue = $config->get($confName . '_cdn');
        if (!empty($confValue) && $libraryFormat) {
          $confValue = [
            $confValue => [
              'type' => 'external',
              'scope' => 'header',
              'attributes' => $attributes,
            ],
          ];
        }
      }
      elseif ($libraryFormat) {
        $confValue = [$confValue => ['scope' => 'header', 'attributes' => $attributes]];
      }
    }
    else {
      $confValue = $config->get($confName . '_cdn');
      if (!empty($confValue) && $libraryFormat) {
        $confValue = [
          $confValue => [
            'type' => 'external',
            'scope' => 'header',
            'attributes' => $attributes,
          ],
        ];
      }
    }

    return $confValue;
  }

  /**
   * Returns CDN Setka Editor state value if local is empty.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Immutable config.
   * @param \Drupal\Core\State\State $state
   *   Drupal state service.
   * @param string $confName
   *   Config value name.
   * @param bool $libraryFormat
   *   TRUE - returns library formatted array, FALSE - returns URL string.
   * @param bool $loadAsync
   *   Load library asynchronously or not.
   *
   * @return string|null
   *   Config value.
   */
  public static function getStateValue(ImmutableConfig $config, State $state, $confName, $libraryFormat = FALSE, $loadAsync = FALSE) {
    $attributes = [];
    if ($loadAsync) {
      $attributes = ['async' => TRUE];
    }

    $setkaUseCdn = TRUE;
    if (\Drupal::lock()->lockMayBeAvailable('setka_editor_files_update')) {
      $setkaUseCdn = $config->get('setka_use_cdn');
    }

    if (!$setkaUseCdn) {
      $value = $state->get($confName);
      if (empty($value)) {
        $value = $state->get($confName . '_cdn');
        if (!empty($value) && $libraryFormat) {
          $value = [
            $value => [
              'type' => 'external',
              'scope' => 'header',
              'attributes' => $attributes,
            ],
          ];
        }
      }
      elseif ($libraryFormat) {
        $value = [$value => ['scope' => 'header', 'attributes' => $attributes]];
      }
    }
    else {
      $value = $state->get($confName . '_cdn');
      if (!empty($value) && $libraryFormat) {
        $value = [
          $value => [
            'type' => 'external',
            'scope' => 'header',
            'attributes' => $attributes,
          ],
        ];
      }
    }

    return $value;
  }

  public function getQuestion() {
    return $this->t('Are you sure you want to change the settings?');
  }

  public function getCancelUrl() {
    return new Url('setka_editor.admin_settings');
  }

  public function getDescription() {
    return $this->t('The new settings will only apply to new content created in Setka Editor. If you want to apply a new style to the previously created content in Setka Editor, you will need to resave it.');
  }

  public function getConfirmText() {
    return $this->t('Confirm');
  }

  public function getCancelText() {
    return $this->t('Cancel');
  }

  public function getFormName() {
    return 'setka_editor_admin_settings_confirm_form';
  }

}
