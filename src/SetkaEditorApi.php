<?php

namespace Drupal\setka_editor;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Uri;

/**
 * Setka Editor API service.
 */
class SetkaEditorApi {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Setka Editor license key.
   *
   * @var string
   */
  protected $licenseKey;

  /**
   * Drupal messenger interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal logger channel interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Setka Editor helper service.
   *
   * @var \Drupal\setka_editor\SetkaEditorHelper
   */
  protected $setkaEditorHelper;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $http_client, MessengerInterface $messenger, ConfigFactory $configFactory, SetkaEditorHelper $setkaEditorHelper, ModuleExtensionList $extension_list_module) {
    $config = $configFactory->get('setka_editor.settings');
    $this->messenger = $messenger;
    $this->logger = $this->getLogger('setka_editor');
    $this->licenseKey = $config->get('setka_license_key');
    $this->httpClient = $http_client;
    $this->setkaEditorHelper = $setkaEditorHelper;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * Returns current build API URL.
   *
   * @return string
   *   Current Build API URL.
   */
  public function getCurrentBuildUrl() {
    return $this->setkaEditorHelper->getSetkaEditorApiDomain() . '/api/v1/custom/builds/current';
  }

  /**
   * Returns push system info API URL.
   *
   * @return string
   *   Push system info API URL.
   */
  public function getPushSystemInfoUrl() {
    return $this->setkaEditorHelper->getSetkaEditorApiDomain() . '/api/v1/drupal/current_theme.json';
  }

  /**
   * Returns icons and fonts API URL.
   *
   * @return string
   *   Icons and fonts API URL.
   */
  public function getIconsFontsUrl() {
    return $this->setkaEditorHelper->getSetkaEditorApiDomain() . '/api/v1/wordpress/files.json';
  }


  /**
   * Returns current setka editor build data.
   *
   * @param string $licenseKey
   *   License key.
   *
   * @return bool|mixed
   *   Current setka editor build data or FALSE on error.
   */
  public function getCurrentBuild($licenseKey = NULL) {
    $queryLicenseKey = $licenseKey ?? $this->licenseKey;

    try {
      $uri = new Uri($this->getCurrentBuildUrl());
      $uri = Uri::withQueryValue($uri, 'token', $queryLicenseKey);
      $response = $this->httpClient->request('GET', $uri);
      if ($response->getStatusCode() == 200) {
        $responseContent = $response->getBody()->getContents();
        if (!empty($responseContent)) {
          return Json::decode($responseContent);
        }
      }
    }
    catch (RequestException $e) {
      $this->logger->error('Get current build error: @error', ['@error' => $e->getMessage()]);
      if ($e->getCode() == 401) {
        $this->messenger->addError($this->t('Invalid license key.'));
      }
      else {
        $response = $e->getResponse();
        $this->messenger->addError($response->getStatusCode() . ': ' . $response->getReasonPhrase());
      }
    }

    return FALSE;
  }

  /**
   * Returns icons and fonts for setka editor.
   *
   * @param string $licenseKey
   *   License key.
   *
   * @return bool|mixed
   *   Icons and fonts or FALSE on error.
   */
  public function getIconsFonts($licenseKey = NULL) {
    $queryLicenseKey = $licenseKey ?? $this->licenseKey;

    try {
      $uri = new Uri($this->getIconsFontsUrl());
      $uri = Uri::withQueryValue($uri, 'token', $queryLicenseKey);
      $response = $this->httpClient->request('GET', $uri);
      if ($response->getStatusCode() == 200) {
        $responseContent = $response->getBody()->getContents();
        if (!empty($responseContent)) {
          return Json::decode($responseContent);
        }
      }
    }
    catch (RequestException $e) {
      $this->logger->error('Get icons and fonts error: @error', ['@error' => $e->getMessage()]);
      if ($e->getCode() == 401) {
        $this->messenger->addError($this->t('Invalid license key.'));
      }
      else {
        $response = $e->getResponse();
        $this->messenger->addError($response->getStatusCode() . ': ' . $response->getReasonPhrase());
      }
    }

    return FALSE;
  }

  /**
   * Pushes information about plugin, site and CMS to Style Manager.
   *
   * @param string $licenseKey
   *   Setka Editor license key.
   */
  public function pushSystemInfo($licenseKey = NULL) {
    global $base_url;

    $queryLicenseKey = $licenseKey ?? $this->licenseKey;
    $moduleInfo = $this->moduleExtensionList->getExtensionInfo('setka_editor');
    $appVersion = \Drupal::VERSION;
    $pluginVersion = $moduleInfo['version'];
    $pluginVersion = empty($pluginVersion) ? '8.x-1.0' : $pluginVersion;
    $domain = $base_url;

    try {
      $uri = new Uri($this->getPushSystemInfoUrl());
      $uri = Uri::withQueryValue($uri, 'token', $queryLicenseKey);
      $uri = Uri::withQueryValue($uri, 'app_version', $appVersion);
      $uri = Uri::withQueryValue($uri, 'plugin_version', $pluginVersion);
      $uri = Uri::withQueryValue($uri, 'domain', $domain);
      $this->httpClient->request('POST', $uri);
    }
    catch (RequestException $e) {
      $this->logger->error('System info push error: @error', ['@error' => $e->getMessage()]);
    }
  }

}
