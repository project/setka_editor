<?php

namespace Drupal\setka_editor\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'setka_editor' formatter.
 *
 * @FieldFormatter(
 *   id = "setka_editor",
 *   module = "setka_editor",
 *   label = @Translation("Setka Editor"),
 *   field_types = {
 *     "text_long",
 *     "string_long",
 *     "text_with_summary",
 *   }
 * )
 */
class SetkaEditorFormatter extends FormatterBase {

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Setka Editor config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $setkaEditorConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    ConfigFactory $configFactory,
    State $state
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->setkaEditorConfig = $configFactory->get('setka_editor.settings');
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $itemValue = $item->value;
      $setkaFormat = FALSE;
      if ($decodedValue = json_decode($itemValue)) {
        if (!empty($decodedValue->postTheme)
          && !empty($decodedValue->postGrid)
          && !empty($decodedValue->postHtml)) {
          $setkaFormat = TRUE;

          $library = [
            'library' => [
              'setka_editor/setka-public-js',
            ]
          ];
          $integrationType = $this->setkaEditorConfig->get('setka_editor_type_of_integration');
          $standaloneStyles = $this->state->get('setka_editor_standalone_styles');
          // If integration type is set to default, load standalone styles.
          if ($integrationType === 'default') {
            // Common files.
            foreach ($standaloneStyles['common'] as $style) {
              $library['library'][] = "setka_editor/setka-styles-common-{$style['id']}";
            }

            // Theme files only which is used in post.
            foreach ($standaloneStyles['themes'] as $style) {
              if ($style['id'] === $decodedValue->postTheme) {
                $library['library'][] = "setka_editor/setka-styles-themes-{$decodedValue->postTheme}";
              }
            }

            // Layouts files only which is used in post.
            foreach ($standaloneStyles['layouts'] as $style) {
              if ($style['id'] === $decodedValue->postGrid) {
                $library['library'][] = "setka_editor/setka-styles-layouts-{$decodedValue->postGrid}";
              }
            }
          }
          else {
            $library['library'][] = 'setka_editor/setka-styles';
          }

          $elements[$delta] = [
            '#theme' => 'setka_editor_formatter',
            '#editor_content' => $decodedValue->postHtml,
            '#attached' => $library,
          ];

          if (!empty($decodedValue->postTypeKit)) {
            $elements[$delta]['#attached']['html_head_link'][] = [[
              'rel' => 'stylesheet',
              'media' => 'all',
              'href' => 'https://use.typekit.net/' . $decodedValue->postTypeKit . '.css',
            ]];
          }
        }
      }
      if (!$setkaFormat) {
        $elements[$delta] = [
          '#type' => 'processed_text',
          '#text' => $item->value,
          '#format' => $item->format,
          '#langcode' => $item->getLangcode(),
        ];
      }
    }

    return $elements;
  }

}
