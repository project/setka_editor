<?php

/**
 * @file
 * Setka Editor AMP module file.
 */

use Drupal\Core\Cache\Cache;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\setka_editor\SetkaEditorHelper;
use Lullabot\AMP\Spec\AttrSpec;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function setka_editor_amp_form_setka_editor_admin_settings_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'setka_editor_amp_admin_settings_form_submit';
}

/**
 * Submit handler for 'setka_editor_admin_settings_form' form.
 */
function setka_editor_amp_admin_settings_form_submit($form, FormStateInterface $form_state) {
  $setka_license_key = $form_state->getValue('setka_license_key');

  if (!empty($setka_license_key)) {

    // Check Setka Editor licence has enabled AMP styles.
    $editorApi = \Drupal::service('setka_editor.api');

    if ($currentBuild = $editorApi->getCurrentBuild($setka_license_key)) {
      setka_editor_amp_styles_update($currentBuild);
    }

  }
}

/**
 * @param array $currentBuild
 */
function setka_editor_amp_styles_update(array $currentBuild) {
  if (!isset($currentBuild['amp_styles']) || empty($currentBuild['amp_styles'])) {
    $link = Link::fromTextAndUrl('https://editor.setka.io/support', Url::fromUri('https://editor.setka.io/support'))->toString();
    \Drupal::messenger()->addWarning(t('Seems that AMP support not enabled. Please, contact customer support: @link.', ['@link' => $link]));
  }
  else {
    $config = \Drupal::config('setka_editor.settings');
    $downloadFiles = (SetkaEditorHelper::checkSetkaFolderPermissions(\Drupal::service('file_system')));
    $ampStyles = setka_editor_amp_parse_amp_styles($currentBuild['amp_styles']);
    $configFactory = \Drupal::configFactory();
    $state = \Drupal::state();

    if ($downloadFiles) {
      $queue = \Drupal::queue('update_setka_editor_amp');
      if (!$queue->numberOfItems()) {
        $queue->createQueue();
      }
      $queue->createItem(['ampStyles' => $ampStyles]);

      $lock = \Drupal::lock();
      if ($lock->acquire('setka_editor_amp_files_update')) {

        while ($newSettingsItem = $queue->claimItem()) {
          $newSettingsData = $newSettingsItem->data['ampStyles'];

          setka_editor_amp_styles_update_task_build($config, $state, $newSettingsData);

          $configFactory->getEditable('setka_editor.settings')
            ->set('amp_styles', $newSettingsData)
            ->save();

          \Drupal::service('library.discovery')->clearCachedDefinitions();
          $configFactory->reset('setka_editor.settings');

          setka_editor_amp_styles_update_task_run($state);

          $queue->deleteItem($newSettingsItem);
        }

        foreach (Cache::getBins() as $cache_backend) {
          $cache_backend->deleteAll();
        }
        \Drupal::service('library.discovery')->clearCachedDefinitions();
        $configFactory->reset('setka_editor.settings');
        \Drupal::service('cache.discovery')->deleteAll();
        \Drupal::service('asset.css.collection_optimizer')->deleteAll();
        _drupal_flush_css_js();

        $lock->release('setka_editor_amp_files_update');
      }
    }
    else { // !$downloadFiles
      $configFactory->getEditable('setka_editor.settings')
        ->set('amp_styles', $ampStyles)
        ->save();
      $state->setMultiple(['amp' => FALSE]);
      foreach (Cache::getBins() as $cache_backend) {
        $cache_backend->deleteAll();
      }

      \Drupal::service('library.discovery')->clearCachedDefinitions();
      $configFactory->reset('setka_editor.settings');
      \Drupal::service('cache.discovery')->deleteAll();
      \Drupal::service('asset.css.collection_optimizer')->deleteAll();
      _drupal_flush_css_js();
    }
  }
}

/**
 * Convert received amp_styles structure to the settings format.
 *
 * @param array $styles
 *   Received styles.
 *
 * @return array
 *   Converted styles.
 */
function setka_editor_amp_parse_amp_styles(array $styles) {
  $parsed = [];

  foreach ($styles as $section => $style) {
    $parsed[$section] = [];

    foreach ($style as $file) {
      $parsed[$section][$file['id']][$file['filetype']] = $file['url'];
      if (isset($file['fonts'])) {
        $parsed[$section][$file['id']]['fonts'] = $file['fonts'];
      }
    }
  }

  return $parsed;
}

/**
 * Implements hook_cron().
 */
function setka_editor_amp_cron() {
  $config = \Drupal::config('setka_editor.settings');
  $ampStyles = $config->get('amp_styles');
  if (!isset($ampStyles) || empty($ampStyles)) {
    return;
  }

  $setka_use_cdn = $config->get('setka_use_cdn');
  $setka_license_key = $config->get('setka_license_key');
  if (!$setka_license_key || $setka_use_cdn) {
    return;
  }

  $editorApi = \Drupal::service('setka_editor.api');
  if ($currentBuild = $editorApi->getCurrentBuild($setka_license_key)) {
    setka_editor_amp_styles_update($currentBuild);
  }
}

/**
 * Sets task of URLs to download.
 *
 * @param \Drupal\Core\Config\ImmutableConfig|\Drupal\Core\Config\Config $config
 *   Drupal config object.
 * @param \Drupal\Core\State\StateInterface $drupalState
 *   Drupal state service.
 * @param array $newSettings
 *   New Style Manager settings.
 */
function setka_editor_amp_styles_update_task_build($config, StateInterface $drupalState, array $newSettings) {
  $updateTask = $drupalState->get('setka_editor_amp_styles_update_task');
  if (empty($updateTask)) {
    $updateTask = [];
  }
  foreach ($newSettings as $section => $styles) {
    $libraryId = 'amp.' . $section;
    foreach ($styles as $styleId => $data) {
      if (count($styles) > 1) {
        $libraryId = 'amp.' . $section . '.' . $styleId;
      }
      $updateTask[$libraryId] = $data['css'];
    }
  }
  $drupalState->set('setka_editor_amp_styles_update_task', $updateTask);
}

/**
 * Update Style Editor AMP styles on server storage.
 *
 * @param StateInterface $drupalState
 *   Drupal state service.
 */
function setka_editor_amp_styles_update_task_run(StateInterface $drupalState) {
  $updateTask = $drupalState->get('setka_editor_amp_styles_update_task');
  if (empty($updateTask)) {
    return;
  }
  $result = ['ampStyles'];
  foreach ($updateTask as $configName => $fileUrl) {
    if ($localFileUrl = setka_editor_amp_styles_download_file($fileUrl)) {
      $result['ampStyles'][$configName] = $localFileUrl;
      unset($updateTask[$configName]);
    }
    else {
      $result['ampStyles'][$configName] = FALSE;
      \Drupal::logger('setka_editor_amp')->error('Unable to download Setka Editor AMP file: @url', ['@url' => $fileUrl]);
    }
  }
  if (!empty($updateTask)) {
    $drupalState->set('setka_editor_amp_styles_update_task', $updateTask);
  }
  else {
    $drupalState->delete('setka_editor_amp_styles_update_task');
  }
  $drupalState->setMultiple($result);
}

/**
 * Downloads and returns internal file URL.
 *
 * @param string $fileUrl
 *   File CDN URL.
 *
 * @return string
 *   Internal file URL.
 */

function setka_editor_amp_styles_download_file($fileUrl) {
  $fileData = file_get_contents($fileUrl);
  if ($fileData) {
    preg_match('/.*?\/([^\/]+)$/', $fileUrl, $matches);
    if ($matches[1]) {
      $file = file_save_data($fileData, 'public://setka/' . $matches[1], FileSystemInterface::EXISTS_REPLACE);
      return $file ? file_create_url($file->getFileUri()) : FALSE;
    }
  }
  return FALSE;
}

/**
 * Build style libraries attached to the amp pages.
 *
 * @param $libraries
 *   Libraries array.
 */
function setka_editor_amp_build_libraries(&$libraries) {
  $config = \Drupal::config('setka_editor.settings');
  $ampStyles = $config->get('amp_styles');

  if (empty($ampStyles)) {
    return;
  }

  $cachedAmpStyles = [];
  if (\Drupal::lock()->lockMayBeAvailable('setka_editor_amp_files_update')) {
    $cachedAmpStyles = \Drupal::state()->get('ampStyles');
  }

  foreach ($ampStyles as $section => $styles) {
    $libraryId = 'amp.' . $section;
    foreach ($styles as $styleId => $data) {
      if (count($styles) > 1) {
        $libraryId = 'amp.' . $section . '.' . $styleId;
      }

      $libraries[$libraryId] = [
        'version' => 'setka_editor',
      ];

      if (!empty($cachedAmpStyles[$libraryId])) {
        $cachedAmpStyle = file_url_transform_relative($cachedAmpStyles[$libraryId]);
        $libraries[$libraryId]['css'] = [
          'theme' => [
            $cachedAmpStyle => [
              'preprocess' => false,
            ],
          ],
        ];
      }
      else {
        $libraries[$libraryId]['css'] = [
          'theme' => [
            $data['css'] => [
              'type' => 'external',
              'preprocess' => false,
            ],
          ],
        ];
      }
      if (!empty($data['fonts'])) {
        foreach ($data['fonts'] as $font) {
          $libraries[$libraryId]['css']['base'][$font] = [
            'type' => 'external',
          ];
        }
      }
    }
  }
}

/**
 * Implements hook_library_info_build().
 */
function setka_editor_amp_library_info_build() {
  $libraries = [];
  setka_editor_amp_build_libraries($libraries);
  return $libraries;
}

/**
 * Implements hook_library_info_alter().
 */
function setka_editor_amp_library_info_alter(&$libraries, $extension) {
  if ($extension !== 'setka_editor_amp') {
    return;
  }
  setka_editor_amp_build_libraries($libraries);
}

/**
 * Implements hook_amp_rules_alter().
 */
function setka_editor_amp_amp_rules_alter(&$rules) {
  foreach ($rules->tags as $tag) {
    // Allow attribute 'style' for tags.
    $styleAttr = new AttrSpec();
    $styleAttr->name = 'style';

    $tag->attrs[] = $styleAttr;
  }
}
