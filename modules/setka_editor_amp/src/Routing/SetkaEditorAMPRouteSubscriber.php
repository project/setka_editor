<?php

namespace Drupal\setka_editor_amp\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class SetkaEditorAMPRouteSubscriber
 *
 * @package Drupal\setka_editor_amp\Routing
 */
class SetkaEditorAMPRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('setka_editor.editor_config_uri')) {
      $route->setDefault('_controller', '\Drupal\setka_editor_amp\Controller\SetkaEditorAMPApiController::editorConfig');
    }

  }

}
