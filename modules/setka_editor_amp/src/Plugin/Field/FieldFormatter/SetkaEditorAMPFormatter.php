<?php

namespace Drupal\setka_editor_amp\Plugin\Field\FieldFormatter;

use Drupal\amp\Service\AMPService;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\setka_editor\Plugin\Field\FieldFormatter\SetkaEditorFormatter;
use Drupal\setka_editor_amp\SetkaAmpSanitizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'setka_editor_amp' formatter.
 *
 * @FieldFormatter(
 *   id = "setka_editor_amp",
 *   module = "setka_editor_amp",
 *   label = @Translation("Setka Editor AMP"),
 *   field_types = {
 *     "text_long",
 *     "string_long",
 *     "text_with_summary",
 *   }
 * )
 */
class SetkaEditorAMPFormatter extends SetkaEditorFormatter implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * @var \Drupal\amp\Service\AMPService
   */
  protected $ampService;

  /**
   * @var \Drupal\setka_editor_amp\SetkaAmpSanitizer
   */
  protected $sanitizer;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AMPService $amp_service, SetkaAmpSanitizer $sanitizer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->ampService = $amp_service;
    $this->sanitizer = $sanitizer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('amp.utilities'),
      $container->get('setka_editor_amp.sanitizer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as &$element) {
      // Remove standard styles.
      if (($key = \array_search('setka_editor/setka-styles', $element['#attached']['library'], TRUE)) !== FALSE) {
        unset($element['#attached']['library'][$key]);
      }

      $this->elementAMPConverter($element);
    }
    unset($element);

    // Attach AMP styles.
    foreach ($items as $delta => $item) {
      if (($decodedValue = Json::decode($item->value)) && !empty($decodedValue['postTheme']) && !empty($decodedValue['postGrid'])) {
        // The order is important.
        $elements[$delta]['#attached']['library'][] = 'setka_editor_amp/amp.common';
        $elements[$delta]['#attached']['library'][] = 'setka_editor_amp/amp.themes.' . $decodedValue['postTheme'];
        $elements[$delta]['#attached']['library'][] = 'setka_editor_amp/amp.layouts.' . $decodedValue['postGrid'];
      }
    }

    return $elements;
  }

  /**
   * Convert elements into AMP.
   *
   * @param array $element
   */
  protected function elementAMPConverter(array &$element): void {
    try {
      $element['#editor_content'] = $element['#editor_content'] ?? $element['#text'] ?? '';
      $dom = new \DOMDocument();
      @$dom->loadHTML(\mb_convert_encoding($element['#editor_content'], 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

      $this->sanitizer->processImages($element, $dom);
      $this->sanitizer->processEmbeds($element, $dom);

      // Export HTML for default AMP converter.
      $element['#editor_content'] = $dom->saveHTML($dom->documentElement);

      /** @var \Drupal\amp\AMP\DrupalAMP $amp */
      $amp = $this->ampService->createAMPConverter();
      $amp->loadHtml($element['#editor_content'], [
        'use_html5_parser' => FALSE,
        'img_max_fixed_layout_width' => 300,
      ]);
      $element['#editor_content'] = $amp->convertToAmpHtml();

      @$dom->loadHTML(\mb_convert_encoding($element['#editor_content'], 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $this->sanitizer->processAnimations($element, $dom);
      $this->sanitizer->processGalleries($element, $dom);

      // Export final (AMP-ready) HTML.
      $element['#editor_content'] = $dom->saveHTML($dom->documentElement);
    }
    catch (\Exception $e) {
      $this->getLogger('setka_editor_amp')->error('Content conversion to AMP failed: ' . $e->getMessage());
    }

    $libraries = [];
    if (!empty($amp->getComponentJs())) {
      $libraries = $this->ampService->addComponentLibraries($amp->getComponentJs());
    }
    $element['#attached']['library'] = \array_merge($element['#attached']['library'], $libraries);
  }

}
