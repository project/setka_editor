<?php

namespace Drupal\setka_editor_amp;

use Drupal\Component\Serialization\Json;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

/**
 * Class SetkaAmpSanitizer
 *
 * @package Drupal\setka_editor_amp
 */
class SetkaAmpSanitizer {

  /**
   * @param $element
   * @param \DOMDocument $dom
   */
  public function processImages(&$element, \DOMDocument $dom): void {
    $xpath = new \DOMXPath($dom);
    $images = $xpath->query('.//img');

    foreach ($images as $image) {
      if (!\is_a($image, \DOMElement::class)) {
        continue;
      }

      // Remove useless class.
      $classes = \trim($image->getAttribute('class'));
      if (!empty($classes)) {
        $classes = \explode(' ', $classes);

        $index = \array_search('stk-reset', $classes, TRUE);
        if ($index !== FALSE) {
          unset($classes[$index]);
          $image->setAttribute('class', \implode(' ', $classes));
        }
      }

      // Set Height and Width for local images.
      if (!$image->getAttribute('width') && !$image->getAttribute('height') && $id = $image->getAttribute('data-image-id')) {
        $entityImage = File::load($id);

        if ($entityImage instanceof FileInterface) {
          $info = getimagesize($entityImage->getFileUri());

          if (!empty($info[0])) {
            $image->setAttribute('width', $info[0]);
          }
          if (!empty($info[1])) {
            $image->setAttribute('height', $info[1]);
          }
        }
      }

      // AMP Library does not process SVG images, so processed by myself.
      $src = \trim($image->getAttribute('src'));
      if (\preg_match('/\.svg$/', $src)) {

        // Set fake height to image. It will be overwritten by styles.
        if (!$image->getAttribute('height')) {
          $image->setAttribute('height', '1em');
        }

        $ampImage = $dom->createElement('amp-img');
        foreach ($image->attributes as $attribute) {
          $ampImage->setAttribute($attribute->name, $attribute->value);
        }

        $image->parentNode->replaceChild($ampImage, $image);
      }
    }

    $element['#attached']['library'][] = 'amp/amp.image';
  }

  /**
   * @param $element
   * @param \DOMDocument $dom
   */
  public function processGalleries(&$element, \DOMDocument $dom): void {
    $xpath = new \DOMXPath($dom);
    $galleries = $xpath->query('//div[contains(@class, "stk-gallery")]');

    foreach ($galleries as $gallery) {
      $maxWidth = 600;
      $maxHeight = 400;
      $ampCarousel = $dom->createElement('amp-carousel');

      $ampCarousel->setAttribute('layout', 'responsive');
      $ampCarousel->setAttribute('type', 'slides');

      foreach ($gallery->childNodes as $child) {
        if (!\is_a($child, \DOMElement::class)) {
          continue;
        }

        $newChild = $child->cloneNode(TRUE);
        $image = $xpath->query('.//amp-img', $newChild)->item(0);

        if ($image) {
          $imageWidth = $image->getAttribute('width');
          $imageHeight = $image->getAttribute('height');
          $image->setAttribute('layout', 'responsive');

          if ($maxWidth < $imageWidth) {
            $maxWidth = $imageWidth;
          }
          if ($maxHeight < $imageHeight) {
            $maxHeight = $imageHeight;
          }
        }

        $ampCarousel->appendChild($newChild);
      }

      $ampCarousel->setAttribute('width', $maxWidth);
      $ampCarousel->setAttribute('height', $maxHeight);

      $gallery->parentNode->replaceChild($ampCarousel, $gallery);
    }

    $element['#attached']['library'][] = 'amp/amp.carousel';
  }

  /**
   * @param $element
   * @param \DOMDocument $dom
   */
  public function processEmbeds(&$element, \DOMDocument $dom): void {
    $xpath = new \DOMXPath($dom);
    $nodes = $xpath->query('//*[contains(@class, \'stk-code_keep-ratio\')]');

    foreach ($nodes as $node) {
      if (!\is_a($node, \DOMElement::class)) {
        continue;
      }

      $attributeValues = $node->getAttribute('class');
      $attributeValues = \explode(' ', $attributeValues);
      $index = \array_search('stk-code_keep-ratio', $attributeValues, TRUE);
      if ($index !== FALSE) {
        unset($attributeValues[$index]);
        $node->setAttribute('class', \implode(' ', $attributeValues));
      }

      $childNodes = $xpath->query('.//*[contains(@class, \'stk-code\')]', $node);
      foreach ($childNodes as $childNode) {
        if (!\is_a($childNode, \DOMElement::class)) {
          continue;
        }

        $childNode->removeAttribute('style');
      }

      $childNodes = $xpath->query('.//iframe', $node);
      foreach ($childNodes as $childNode) {
        if (!\is_a($childNode, \DOMElement::class)) {
          continue;
        }

        $childNode->setAttribute('layout', 'responsive');
      }
    }

    $element['#attached']['library'][] = 'amp/amp.iframe';
  }

  /**
   * @param $element
   * @param \DOMDocument $dom
   */
  public function processAnimations(&$element, \DOMDocument $dom): void {
    $xpath = new \DOMXPath($dom);

    $nodes = $xpath->query('//*[@data-anim="true"]');
    $animationsExists = FALSE;

    $attributesToRemove = [
      'data-anim',
      'data-anim-delay',
      'data-anim-direction',
      'data-anim-duration',
      'data-anim-loop',
      'data-anim-opacity',
      'data-anim-rotation',
      'data-anim-shift',
      'data-anim-trigger',
      'data-anim-zoom',
      'data-anim-played',
      'data-anim-name',
    ];

    foreach ($nodes as $key => $node) {
      $animation = $this->prepareAnimationConfig($node, $key);

      $cssClasses = $node->getAttribute('class');
      $cssClasses .= ' stk-anim ' . $animation['id'];
      $node->setAttribute('class', $cssClasses);

      $id = $node->hasAttribute('id') ? $node->getAttribute('id') : 'target-' . $animation['id'];
      $node->setAttribute('id', $id);

      foreach ($attributesToRemove as $attribute) {
        $node->removeAttribute($attribute);
      }

      try {
        $positionObserver = $this->createPositionObserver($id, $animation, $dom);
        $animationElement = $this->createAnimationElement($animation, $dom);
        $this->createAnimationStylesElement($key, $animation);

        $dom->documentElement->appendChild($positionObserver);
        $dom->documentElement->appendChild($animationElement);
        $animationsExists = TRUE;
      }
      catch (\Exception $e) {
        // Make a log entry, skip the element.
        \watchdog_exception('setka_editor_amp', $e);
      }
    }

    if ($animationsExists) {
      $runtimeStyles = &drupal_static(SetkaAmpSanitizer::class, []);
      $runtimeStyles[] = '.stk-post.stk-post .stk-anim.stk-anim { transform: translateX(var(--stk-shift-x, 0)) translateY(var(--stk-shift-y, 0)) rotate(var(--stk-rotation, 0)) scale(var(--stk-zoom, 1)); opacity: var(--stk-opacity, 1); }';

      $element['#attached']['library'][] = 'amp/amp.position-observer';
      $element['#attached']['library'][] = 'amp/amp.animation';
    }
  }

  /**
   * @param \DOMElement $node
   * @param $number
   *
   * @return array
   */
  protected function prepareAnimationConfig(\DOMElement $node, $number): array {
    $animation = [];

    $animation['id'] = 'stk-anim-' . \abs((int) $number);
    $animation['selector'] = '.' . $animation['id'];
    $animation['duration'] = (float) $node->getAttribute('data-anim-duration') * 1000;
    $animation['delay'] = (float) $node->getAttribute('data-anim-delay') * 1000;

    $direction = $node->getAttribute('data-anim-direction');
    $shift = $node->getAttribute('data-anim-shift');

    if ($direction === 'bottom' || $direction === 'right') {
      $shift *= -1;
    }
    $shift .= 'px';

    if ($direction === 'top' || $direction === 'bottom') {
      $animation['--stk-shift-y'] = $shift;
      $animation['--stk-shift-x'] = '0px';
    }
    else {
      $animation['--stk-shift-x'] = $shift;
      $animation['--stk-shift-y'] = '0px';
    }

    $animation['--stk-zoom'] = (float) $node->getAttribute('data-anim-zoom') / 100;
    $animation['--stk-rotation'] = (float) $node->getAttribute('data-anim-rotation') . 'deg';
    $animation['--stk-opacity'] = (float) $node->getAttribute('data-anim-opacity') / 100;

    $animation['keyframes'] = [
      'transform' => [
        'translateX(var(--stk-shift-x, 0)) translateY(var(--stk-shift-y, 0)) rotate(var(--stk-rotation, 0)) scale(var(--stk-zoom, 1))',
        'translate(0, 0) scale(1)',
      ],
      'opacity' => ['var(--stk-opacity, 1)', 1],
    ];

    return $animation;
  }

  /**
   * @param $id
   * @param array $animation
   * @param \DOMDocument $dom
   *
   * @return \DOMElement
   */
  protected function createPositionObserver($id, array $animation, \DOMDocument $dom): \DOMElement {
    $node = $dom->createElement('amp-position-observer');
    $node->setAttribute('on', 'enter:' . $animation['id'] . '.start;');
    $node->setAttribute('intersection-ratios', '0 0.5');
    $node->setAttribute('layout', 'nodisplay');
    $node->setAttribute('target', $id);

    return $node;
  }

  /**
   * @param array $animation
   * @param \DOMDocument $dom
   *
   * @return \DOMElement
   */
  protected function createAnimationElement(array $animation, \DOMDocument $dom): \DOMElement {
    $config = [
      'fill' => 'both',
      'easing' => 'ease',
      'iterations' => 1,
      'animations' => [
        $animation,
      ],
    ];

    $json = Json::encode($config);

    $node = $dom->createElement('amp-animation');
    $node->setAttribute('id', $animation['id']);
    $node->setAttribute('layout', 'nodisplay');

    $script = $dom->createElement('script');
    $script->setAttribute('type', 'application/json');
    $script->textContent = $json;

    $node->appendChild($script);

    return $node;
  }

  /**
   * @param $index
   * @param array $animation
   */
  protected function createAnimationStylesElement($index, array $animation): void {
    $properties = [
      '--stk-shift-y' => NULL,
      '--stk-shift-x' => NULL,
      '--stk-zoom' => NULL,
      '--stk-rotation' => NULL,
      '--stk-opacity' => NULL,
    ];

    foreach ($properties as $name => $value) {
      if (isset($animation[$name])) {
        $properties[$name] = $animation[$name];
      }
      else {
        unset($properties[$name]);
      }
    }

    $css = '';
    foreach ($properties as $property => $value) {
      $css .= $property . ':' . $value . ';';
    }
    $css = \sprintf('.stk-anim.stk-anim-%s {%s}', $index, $css);

    $runtimeStyles = &drupal_static(SetkaAmpSanitizer::class, []);
    $runtimeStyles[] = $css;
  }

}
