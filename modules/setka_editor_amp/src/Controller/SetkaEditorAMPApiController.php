<?php

namespace Drupal\setka_editor_amp\Controller;

use Drupal\setka_editor\Controller\SetkaEditorApiController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SetkaEditorAMPApiController
 *
 * @package Drupal\setka_editor_amp\Controller
 */
class SetkaEditorAMPApiController extends SetkaEditorApiController {

  /**
   * Extending API gate to get setka editor AMP styles.
   *
   * @see SetkaEditorApiController::editorConfig()
   */
  public function editorConfig(Request $request) {
    $currentBuild = $request->request->get('data');

    if (isset($currentBuild['amp_styles']) && !empty($currentBuild['amp_styles'])) {
      setka_editor_amp_styles_update($currentBuild);
    }

    return parent::editorConfig($request);
  }

}
