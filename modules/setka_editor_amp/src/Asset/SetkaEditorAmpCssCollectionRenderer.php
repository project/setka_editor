<?php

namespace Drupal\setka_editor_amp\Asset;

use Drupal\amp\Asset\AmpCssCollectionRenderer;
use Drupal\setka_editor_amp\SetkaAmpSanitizer;

/**
 * Class SetkaEditorAmpCssCollectionRenderer
 *
 * @package Drupal\setka_editor_amp\Asset
 */
class SetkaEditorAmpCssCollectionRenderer extends AmpCssCollectionRenderer {

  /**
   * {@inheritdoc}
   */
  public function render(array $css_assets) {

    $elements = parent::render($css_assets);

    if (!$this->ampService->isAmpRoute()) {
      return $elements;
    }

    $merged = '';
    $replacement_key = NULL;
    foreach ($elements as $key => $replacement_element) {
      if ($replacement_element['#tag'] === 'style' && $replacement_element['#type'] === 'amp_custom_style') {
        $replacement_key = $key;
        $merged .= $replacement_element['#value'];
        break;
      }
    }

    // Collect runtime CSSs.
    $all_css = drupal_static(SetkaAmpSanitizer::class, []);

    foreach ($css_assets as $css_key => $css_asset) {
      if ($css_asset['version'] === 'setka_editor') {
        $url = \file_url_transform_relative(\file_create_url($css_asset['data']));
        $provider = \parse_url($url, PHP_URL_HOST);

        // Merge only remote amp styles.
        if (!empty($provider)) {

          // Remove already included typekit font.
          if ($provider === 'use.typekit.net') {
            foreach ($elements as $element_key => $element) {
              if ($element['#tag'] === 'link' && $element['#attributes']['href'] === $css_asset['data']) {
                unset($elements[$element_key]);
              }
            }
          }

          if (\in_array($provider, $this->link_domain_whitelist, TRUE)) {
            unset($css_assets[$css_key]);
            continue;
          }

          $css = \file_get_contents($url);
          $css = $this->minify($css);
          $css = $this->strip($css);
          $all_css[] = $css;
        }

        unset($css_assets[$css_key]);
      }
    }

    $value = implode('', $all_css);
    $merged .= $this->minify($value);

    $elements[$replacement_key] = [
      '#tag' => 'style',
      '#type' => 'amp_custom_style',
      '#value' => $merged,
    ];

    return $elements;
  }

}
